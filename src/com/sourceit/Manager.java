package com.sourceit;

import static com.sourceit.Utils.round;

public class Manager extends Thread {

    private static final long TIME = 2 * 1000;

    double usd;
    double uah;

    public Manager(Basket instance) {
        this.usd = instance.getUsd();
        this.uah = instance.getUah();
    }

    @Override
    public void run() {

        while (Basket.getInstance().isActive()) {
            System.out.println("Manager: " + calcIncome() + " uah");

            try {
                Thread.sleep(TIME);
            } catch (InterruptedException e) {
                //DO NOTHING
            }
        }

        System.out.println("Manager: total " + calcIncome() + " uah");
    }

    private double calcIncome() {
        double rentUAH = Basket.getInstance().getUah() - uah;
        rentUAH += (Basket.getInstance().getUsd() - usd) * Basket.SELL;
        return round(rentUAH, 2);
    }
}
