package com.sourceit;

import static com.sourceit.Utils.round;

public class Basket {

    public static final double SELL = 27.2d;
    private static final double BY = 27d;

    private double usd = 10_000;
    private double uah = 500_000;

    private static Basket instance;
    private boolean active = true;
    private int totalOperation;

    public static synchronized Basket getInstance() {
        if (instance == null) {
            instance = new Basket();
        }
        return instance;
    }

    private Basket() {
    }

    public synchronized boolean byUSD(double uah) {
        if (!active || totalOperation > 30) {
            active = false;
            return false;
        }

        System.out.println("uah - " + uah + "(" + getTotal() + ")");

        double tempUsd = round(uah / SELL, 2);
        if (tempUsd < usd) {
            this.uah += uah;
            usd -= tempUsd;
            totalOperation++;
            return true;
        } else {
            active = false;
            return false;
        }
    }

    public synchronized boolean sellUSD(double usd) {
        if (!active || totalOperation > 30) {
            active = false;
            return false;
        }

        System.out.println("usd - " + usd + "(" + getTotal() + ")");

        double tempUah = round(usd * BY, 2);
        if (tempUah < uah) {
            uah -= tempUah;
            this.usd += usd;
            totalOperation++;
            return true;
        } else {
            active = false;
            return false;
        }
    }

    public double getUsd() {
        return usd;
    }

    public double getUah() {
        return uah;
    }

    public boolean isActive() {
        return active;
    }

    public synchronized int getTotal() {
        return totalOperation;
    }
}
