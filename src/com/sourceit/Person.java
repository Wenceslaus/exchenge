package com.sourceit;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class Person extends Thread {

    long time;

    public Person(long time) {
        this.time = time;
    }


    @Override
    public void run() {
        boolean result;
        do {

            try {
                Thread.sleep(time);
            } catch (InterruptedException e) {
                //DO NOTHING
            }

            if (new Random().nextBoolean()) {
                double uah = ThreadLocalRandom.current().nextInt(1000, 100_000);
                result = Basket.getInstance().byUSD(uah);
            } else {
                double usd = ThreadLocalRandom.current().nextInt(100, 5_000);
                result = Basket.getInstance().sellUSD(usd);
            }

        } while (result);
    }
}
