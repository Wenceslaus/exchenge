package com.sourceit;

import java.util.ArrayList;
import java.util.List;

import static java.util.concurrent.TimeUnit.SECONDS;

public class Main {

    public static void main(String[] args) {

        List<Person> persons = new ArrayList<>();

        persons.add(new Person(SECONDS.toMillis(1)));
        persons.add(new Person(SECONDS.toMillis(1)));
        persons.add(new Person(SECONDS.toMillis(2)));
        persons.add(new Person(SECONDS.toMillis(2)));
        persons.add(new Person(SECONDS.toMillis(2)));


        Thread manager = new Manager(Basket.getInstance());
        manager.start();


        for (Person person : persons) {
            person.start();
        }

    }
}
